<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class OpeningTime extends Pivot
{
    public function boutiques()
    {
        return $this->belongsTo('App\Boutique');
    }
}

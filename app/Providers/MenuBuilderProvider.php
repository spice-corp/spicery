<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\ServiceProvider;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class MenuBuilderProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher $events
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            $event->menu->add('ÉLÉMENTS PRINCIPAUX');
            $event->menu->add([
                'text' => 'Items',
                'url' => route("admin_items"),
                'icon' => 'archive',
                'active' => ['admin/items', 'admin/item/*']
            ]);
            $event->menu->add([
                'text' => 'Boutiques',
                'url' => route("admin_boutiques"),
                'icon' => 'shopping-bag',
                'active' => ['admin/boutiques', 'admin/boutique/*']
            ]);
        });
    }
}

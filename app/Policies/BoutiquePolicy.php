<?php

namespace App\Policies;

use App\Boutique;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BoutiquePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the boutique.
     *
     * @param  \App\User $user
     * @param  \App\Boutique $boutique
     * @return mixed
     */
    public function view(User $user, Boutique $boutique)
    {
        return true;
    }

    /**
     * Determine whether the user can create boutiques.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return isset($user);
    }

    /**
     * Determine whether the user can update the boutique.
     *
     * @param  \App\User $user
     * @param  \App\Boutique $boutique
     * @return mixed
     */
    public function update(User $user, Boutique $boutique)
    {
        $isOwner = $boutique->owners->where('id', $user->id)->isNotEmpty();
        return (bool)$user->admin || $isOwner;
    }

    /**
     * Determine whether the user can delete the boutique.
     *
     * @param  \App\User $user
     * @param  \App\Boutique $boutique
     * @return mixed
     */
    public
    function delete(User $user, Boutique $boutique)
    {
        return (bool)$user->admin;
    }

    /**
     * Determine whether the user can restore the boutique.
     *
     * @param  \App\User $user
     * @param  \App\Boutique $boutique
     * @return mixed
     */
    public
    function restore(User $user, Boutique $boutique)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the boutique.
     *
     * @param  \App\User $user
     * @param  \App\Boutique $boutique
     * @return mixed
     */
    public
    function forceDelete(User $user, Boutique $boutique)
    {
        //
    }
}

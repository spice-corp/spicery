<?php

namespace App\Policies;

use App\Item;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ItemPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function update(User $user, Item $item)
    {
        return (bool)$user->admin || $item->owner === $user->id;
    }

    public function create(User $user)
    {
        return isset($user);
    }
}

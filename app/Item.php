<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use App;

class Item extends Model
{

    use Searchable;

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return env('ALGOLIA_ITEMS_INDEX', 'items');
    }


    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $collection = collect($this)->merge(
            ["countryname" => $this->countryName("fr"),
                "avg_price" => $this->averagePrice(),
                "countrycode" => strtolower($this->countrycode),
                "imgthumb_src" => asset($this->imgthumb_src),
                "img_src" => asset($this->img_src)
            ]);
        return $collection->toArray();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function boutiques()
    {
        return $this->belongsToMany('App\Boutique')->withPivot('price');
    }

    public function creator()
    {
        return $this->belongsTo("App\User");
    }

    public function countryName(string $lang = null)
    {
        if (isset($lang)) {
            return \Locale::getDisplayRegion('-' . $this->countrycode, $lang);
        } else {
            return \Locale::getDisplayRegion('-' . $this->countrycode, App::getLocale());
        }
    }

    public function averagePrice()
    {
        return $this->boutiques->avg("pivot.price");
    }
}

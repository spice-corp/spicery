<?php

namespace App;

use App;
use function GuzzleHttp\Psr7\str;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use DB;

class Boutique extends Model
{

    use Searchable;

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return env('ALGOLIA_BOUTIQUES_INDEX', 'boutiques');
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $collection = collect($this)->merge(
            ["countrycode" => $this->majorCountry(),
                "countryname" => $this->countryName("fr")
            ]
        );
        return $collection->toArray();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'lat', 'lng',
    ];

    public function times()
    {
        return $this->hasMany('App\OpeningTime');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function items()
    {
        return $this->belongsToMany('App\Item')->withPivot('price');
    }

    public function owners()
    {
        return $this->belongsToMany('App\User');
    }

    public function countryName(string $lang = null)
    {
        if (isset($lang)) {
            return \Locale::getDisplayRegion('-' . $this->majorCountry(), $lang);
        } else {
            return \Locale::getDisplayRegion('-' . $this->majorCountry(), App::getLocale());
        }
    }

    public function majorCountry()
    {
        $countryStats = DB::table('boutiques')->selectRaw('items.countrycode, COUNT(*) * 100.0 / (SELECT COUNT(*) FROM boutique_item WHERE boutique_item.boutique_id = ?) as percentage', [$this->id])->join("boutique_item", "boutiques.id", "=", "boutique_item.boutique_id")->join("items", "boutique_item.item_id", "=", "items.id")->where("boutique_item.boutique_id", "=", $this->id)->groupBy("items.countrycode")->get();
        //dump($countryStats);
        if (isset($countryStats[0])) {
            return strtolower($countryStats[0]->countrycode);
        }
    }
}

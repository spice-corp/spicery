<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemRequest;
use Illuminate\Http\Request;
use App\Item;
use Illuminate\Support\Facades\Auth;
use Storage;
use Illuminate\Support\Str;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.items.edit");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemRequest $request)
    {
        $data = $request->validated();

        if (!isset($data["image"])) {
            Redirect::route('admin_item_new')
                ->withErrors(["image" => "Merci d'ajouter une image à la création du produit"]);
        }

        $imgPath = Tools\ImageManager::storeItem($data['image'], $data["name"]);
        $imgThumbPath = Tools\ImageManager::storeItemThumb($data['image'], $data["name"]);

        $item = new Item;
        $item->name = $data["name"];
        $item->description = $data["description"];
        $item->countrycode = $data["countrycode"];
        $item->img_src = $imgPath;
        $item->imgthumb_src = $imgThumbPath;
        $item->owner = Auth::id();
        $item->save();
        return redirect()->route("admin_items");
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        return view("description.description_produit")->with("item", $item);
    }

    public function search()
    {
        return view('recherche.recherche')->with("searchMode", "items");
    }

    public function unknownSearch(Request $request)
    {
        if ($request->has("product") && !empty($request->input("product"))) {
            return redirect()->route("products", ["product" => $request->input("product")]);
        } elseif ($request->has("boutique")) {
            return redirect()->route("boutiques", ["boutique" => $request->input("boutique")]);
        } else {
            return redirect('products');
        }
    }

    public function random()
    {
        return redirect()->route("product_product", ['item' => Item::inRandomOrder()->first()->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        return view("admin.items.edit")->with("item", $item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function update(ItemRequest $request, Item $item)
    {
        $data = $request->validated();

        if (isset($data["image"])) {
            $imgPath = Tools\ImageManager::updateItem($data['image'], $data["name"], $item->img_src);
            $imgThumbPath = Tools\ImageManager::updateItemThumb($data['image'], $data["name"], $item->imgthumb_src);
            $item->img_src = $imgPath;
            $item->imgthumb_src = $imgThumbPath;
        }

        $item->name = $data["name"];
        $item->description = $data["description"];
        $item->countrycode = $data["countrycode"];

        $item->save();
        return redirect()->route("admin_items");
    }

    public function deleteConfirm(Item $item)
    {
        return view("admin.items.delete")->with("item", $item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        Storage::delete("public/" . Str::after($item->img_src, "storage/"));
        Storage::delete("public/" . Str::after($item->imgthumb_src, "storage/"));
        $item->delete();
        return redirect()->route("admin_items");
    }
}

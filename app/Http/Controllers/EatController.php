<?php

namespace App\Http\Controllers;

use App\Eat;
use Illuminate\Http\Request;

class EatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Eat  $eat
     * @return \Illuminate\Http\Response
     */
    public function show(Eat $eat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Eat  $eat
     * @return \Illuminate\Http\Response
     */
    public function edit(Eat $eat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Eat  $eat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Eat $eat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Eat  $eat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Eat $eat)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Boutique;
use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.home');
    }

    public function showItems(Request $request)
    {
        $items = Item::all();

        return view('admin.items', ['items' => $items]);
    }

    public function showBoutiques(Request $request)
    {
        $allBoutiques = Boutique::with("tags")->get();

        if (Auth::user()->can("access-admin")) {
            $boutiques = $allBoutiques;
        } else {
            $boutiques = Auth::user()->boutiques()->get();
        }

        return view('admin.boutiques', ['boutiques' => $boutiques]);
    }
}

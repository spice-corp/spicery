<?php

namespace App\Http\Controllers;

use App\Boutique;
use App\Http\Requests\BoutiqueRequest;
use App\Item;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BoutiqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.boutiques.edit")->with("itemList", Item::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BoutiqueRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BoutiqueRequest $request)
    {
        $data = $request->validated();

        $boutique = new Boutique;

        $boutique->name = $data["name"];
        $boutique->description = $data["description"];
        $boutique->phone = $data["phone"];
        $boutique->email = $data["email"];
        $boutique->address = $data["adresse"];
        $boutique->lat = $data["latitude"];
        $boutique->lng = $data["longitude"];
        $boutique->web = $data["web"];
        $boutique->save();

        if (isset($data["items"])) {
            $items = collect($data["items"])->map(function ($value, $key) {
                return collect($value)->only(["price"]);
            });
            $boutique->items()->sync($items);
            $boutique->items()->searchable();
        }

        $boutique->owners()->attach(Auth::id());

        return redirect()->route("admin_boutiques");
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Boutique $boutique
     * @return \Illuminate\Http\Response
     */
    public function show(Boutique $boutique)
    {
        $countryStats = DB::table('boutiques')->selectRaw('items.countrycode, COUNT(*) * 100.0 / (SELECT COUNT(*) FROM boutique_item WHERE boutique_item.boutique_id = ?) as percentage', [$boutique->id])->join("boutique_item", "boutiques.id", "=", "boutique_item.boutique_id")->join("items", "boutique_item.item_id", "=", "items.id")->where("boutique_item.boutique_id", "=", $boutique->id)->groupBy("items.countrycode")->get();
        $countryStatsLabels = $countryStats->pluck("countrycode")->map(function ($value, $key) {
            return \Locale::getDisplayRegion('-' . $value, 'fr');
        })->toJson();
        $countryStatsValues = $countryStats->pluck("percentage")->map(function ($value, $key) {
            return floatval($value);
        })->toJson();
        $sortedItems = $boutique->items->sortByDesc(function ($col) {
            return $col;
        })->values()->all();
        return view("boutiques.boutique")->with("boutique", $boutique)->with("countryStatsLabels", $countryStatsLabels)->with("countryStatsValues", $countryStatsValues)->with("items", $sortedItems);
    }

    public function search()
    {
        return view('recherche.recherche')->with("searchMode", "boutiques");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Boutique $boutique
     * @return \Illuminate\Http\Response
     */
    public function edit(Boutique $boutique)
    {
        return view("admin.boutiques.edit")->with("boutique", $boutique)->with("itemList", Item::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BoutiqueRequest $request
     * @param \App\Boutique $boutique
     * @return \Illuminate\Http\Response
     */
    public function update(BoutiqueRequest $request, Boutique $boutique)
    {
        $data = $request->validated();

        if (isset($data["items"])) {
            $items = collect($data["items"])->map(function ($value, $key) {
                return collect($value)->only(["price"]);
            });
        } else {
            $items = [];
        }
        //dd($items);
        $boutique->items()->sync($items);
        $boutique->items()->searchable();

        $boutique->name = $data["name"];
        $boutique->description = $data["description"];
        $boutique->phone = $data["phone"];
        $boutique->address = $data["adresse"];
        $boutique->lat = $data["latitude"];
        $boutique->lng = $data["longitude"];
        $boutique->web = $data["web"];
        $boutique->save();

        return redirect()->route("admin_boutiques");
    }

    public function deleteConfirm(Boutique $boutique)
    {
        return view("admin.boutiques.delete")->with("boutique", $boutique);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Boutique $boutique
     * @return \Illuminate\Http\Response
     */
    public function destroy(Boutique $boutique)
    {
        $boutique->delete();
        return redirect()->route("admin_boutiques");
    }
}

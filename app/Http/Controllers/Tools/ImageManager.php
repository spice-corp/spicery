<?php

namespace App\Http\Controllers\Tools;

use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ImageManager extends Controller
{
    static public function updateItem(UploadedFile $image, String $name, String $oldimagePath)
    {
        Storage::delete("public/" . Str::after($oldimagePath, "storage/"));
        return self::storeItem($image, $name);
    }

    static public function storeItem(UploadedFile $image, String $name)
    {
        $img = Image::make($image)->resize(1250, 1250, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->encode("jpg", 80);

        $fileName = Str::slug(Str::limit($name, 10)) . Str::uuid() . '.jpg';

        $imgPath = 'imgs/items/' . $fileName;
        Storage::put("public/" . $imgPath, $img);
        return "storage/" . $imgPath;
    }

    static public function updateItemThumb(UploadedFile $image, String $name, String $oldimagePath)
    {
        Storage::delete("public/" . Str::after($oldimagePath, "storage/"));
        return self::storeItemThumb($image, $name);
    }

    static public function storeItemThumb(UploadedFile $image, String $name)
    {
        $imgThumb = Image::make($image)->resize(400, 400, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->encode("jpg", 60);

        $fileName = Str::slug(Str::limit($name, 10)) . Str::uuid() . '_thumb.jpg';

        $imgThumbPath = 'imgs/items/' . $fileName;
        Storage::put("public/" . $imgThumbPath, $imgThumb);
        return "storage/" . $imgThumbPath;
    }
}

<?php

namespace App\Http\Controllers;

use App\Eat;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $eats = Cache::remember('lastEats', 60, function () {
            return Eat::latest()->take(3)->get();
        });

        return view('accueil.accueil')->with("eats", $eats);
    }
}

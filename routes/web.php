<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('admin', 'AdminController@index')->name('admin')->middleware("auth");
Route::prefix("admin")->name("admin_")->middleware("auth")->group(function () {
    Route::get('items', "AdminController@showItems")->name("items");
    Route::prefix("item")->name("item_")->group(function () {
        Route::get('{item}', "ItemController@edit")->where('item', '[0-9]+')->name("edit")->middleware("can:items.update,item");
        Route::post('{item}', "ItemController@update")->where('item', '[0-9]+')->name("edit")->middleware("can:items.update,item");
        Route::get('new', "ItemController@create")->name("new")->middleware("can:items.create");
        Route::post('new', "ItemController@store")->name("new")->middleware("can:items.create");
        Route::get('{item}/delete', "ItemController@deleteConfirm")->where('item', '[0-9]+')->name("delete")->middleware("can:access-admin");
        Route::post('{item}/delete', "ItemController@destroy")->where('item', '[0-9]+')->name("delete")->middleware("can:access-admin");
    });

    Route::get('boutiques', "AdminController@showBoutiques")->name("boutiques");
    Route::prefix("boutique")->name("boutique_")->group(function () {
        Route::get('{boutique}', "BoutiqueController@edit")->where('boutique', '[0-9]+')->name("edit")->middleware("can:boutiques.update,boutique");
        Route::post('{boutique}', "BoutiqueController@update")->where('boutique', '[0-9]+')->name("edit")->middleware("can:boutiques.update,boutique");
        Route::get('new', "BoutiqueController@create")->name("new")->middleware("can:boutiques.create");
        Route::post('new', "BoutiqueController@store")->name("new")->middleware("can:boutiques.create");
        Route::get('{boutique}/delete', "BoutiqueController@deleteConfirm")->where('boutique', '[0-9]+')->name("delete")->middleware("can:access-admin");
        Route::post('{boutique}/delete', "BoutiqueController@destroy")->where('boutique', '[0-9]+')->name("delete")->middleware("can:access-admin");
    });
});
Route::get('/', "HomeController@index")->name("home");

Route::get('products', "ItemController@search")->name("products");
Route::prefix('product')->name("product_")->group(function () {
    Route::get('{item}', "ItemController@show")->where('item', '[0-9]+')->name("product");
    Route::get('random', "ItemController@random")->name("random");
});


Route::get('boutiques', "BoutiqueController@search")->name("boutiques");
Route::prefix('boutique')->name("boutique_")->group(function () {
    Route::get('{boutique}', "BoutiqueController@show")->where('boutique', '[0-9]+')->name("boutique");
});

Route::get('/eats', "EatController@index")->name("eats");

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/search', "ItemController@unknownSearch")->name("search");

Route::get("/about", function () {
    return view("apropos.apropos");
})->name("about");
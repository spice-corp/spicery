<?php

use App\User;
use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('admin:set {user}', function () {
    $retrievedUser = User::find($this->argument('user'));
    if (!isset($retrievedUser)) {
        $retrievedUser = User::where("email", $this->argument('user'))->first();
    }
    if (!isset($retrievedUser)) {
        $this->error("L'utilisateur donné est introuvable");
        return;
    }
    $this->line("Voici les informations de l'utilisateur :");
    $this->table(["ID", "Nom", "E-Mail", "Admin"], [[$retrievedUser->id, $retrievedUser->name, $retrievedUser->email, $retrievedUser->admin ? "True" : "False"]]);
    if ($this->confirm('Voulez-vous continuer ?')) {
        $retrievedUser->admin = !$retrievedUser->admin;
        $retrievedUser->save();
        $this->info("L'utilisateur est maintenant à " . (Int)$retrievedUser->admin);
    }
})->
describe("Permet d'activer (ou de desactiver) le status d'administrateur d'une personne");

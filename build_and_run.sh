#!/bin/sh

mv .env.example .env
shopt -s extglob dotglob
mkdir spicery_app && mv !(.|..|spicery_app|docker-compose.yml|.env|spicery_storage) ./spicery_app/
shopt -u dotglob
echo "Faites vos modifications dans le .env maintenant. Une fois celles-ci effectués, appuyez sur ENTREE"
read a
sudo cp .env spicery_app/
sudo docker-compose --verbose -e -f ./docker-compose.yml build
sudo mkdir spicery_storage
mkdir -p spicery_storage/framework/{sessions,views,cache}
sudo docker-compose --verbose -f ./docker-compose.yml up -d
sudo docker-compose exec app php artisan key:generate
sudo docker-compose exec app php artisan config:cache
sudo docker-compose exec app wait-for-it mariadb:3306 -- php artisan migrate --force
sudo chown 33:33 -R ./spicery_storage
sudo chmod 755 -R ./spicery_storage

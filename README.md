# Spicery

Spicery est une plateforme (open source sous license AGPL) répertoriant les épiceries internationales à Paris. On peut y chercher des produits, et les boutiques, ainsi que diverses informations sur ceux-ci.

# TP Réseau Spicery

# Installation automatique _(Docker, préférée)_

## Prérequis

Pour mettre en production l'application, il vous faut :

-   `Docker` et `Docker Compose` (si possible en dernière version),
-   Un terminal, ou un accès permettant de lancer ces commandes
-   Un compte Algolia, avec deux index : un pour les boutiques, et un pour les produits

## Préparation

1. Il faut d'abord copier le dépôt, renommer le fichier `.env.example` vers `.env`, puis compléter tous les champs nécessaires.
2. Ensuite, il faut modifier le `docker-compose.yml` pour indiquer un mot de passe à paramétrer pour la base de données via `$DB_PASS` (ou également configurer d'autres options, sans oublier de vérifier tous les chaps où \$DB*PASS est présent).  
   **Note :** Si vous utilisez l'intégration continue via le runner GitLab, le mot de passe est fourni via la variable d"environnement `DB_PASS` (paramétrable via GitLab, ou dans `.gitlab-ci.yml`). Il est aussi possible de la passer dans le shell, ce qui permet d'éviter de retaper le mot de passe dans les différentes variables.*

## Installation

Lancez "`build_and_run.sh`" (sans oublier d'ajouter les permissions d'execution) dans le dossier racine du projet. Cela créera le dossier **spicery_app** qui contiendra une copie du projet, prête à fonctionner, un dossier de stockage **spicery_storage** (sauf si la configuration du stockage a été modifiée), ainsi que **db_data** qui contiendra lui les fichiers de la base de données.

<!-- 2. Entrez dans la machine virtuelle via la commande "`docker exec -it spicery_app_1 /bin/bash`" (adapter le nom du conteneur si besoin). Il faut lancer ces commandes :
        - "`php artisan key:generate`" : pour générer la clé qui servira aux algorithmes de hashage,
        - "`php artisan config:cache`" pour mettre la configuration et d'autres éléments en cache,
        - Et enfin "`php artisan migrate`" pour mettre à jour la base de données. -->

Le service est maintenant disponible sur le port `9002` (par défaut). Il reste à mettre en place un reverse proxy si besoin.

# Installation manuelle

Il est possible d'installer cette application manuellement, mais merci de ne pas reporter de bugs dans ce cas. Vous devrez mettre en place un système avec PHP 7.2 minimum, MariaDB/MySQL (ou PostgreSQL avec un petit changement de config), ainsi que Redis pour le cache, les files et les sessions. Il faudra aussi lancer `composer install` et compiler tous les assets de front (SASS et JS) avec Webpack, via Node.JS : `npm run build` .

## Prérequis :

Il faut :

-   Redis (serveur de cache, pour optimiser les performances en évitant certaines requêtes),
-   MySQL ou PostgreSQL (au choix, pour des raisons de performances PostgreSQL est recommandé),
-   Un serveur SMTP (optionnel, uniquement pour l'envoi de mails, comme pour la récupération des mots de passe),
-   Un serveur Nginx avec PHP >= 7.1.3 (avec cette configuration : https://laravel.com/docs/5.8#pretty-urls) (ou Apache qui puisse lancer du PHP en indiquant le dossier "`public`", ) et les extensions suivantes : `OpenSSL` (chiffrement), `PDO` (+ l'extension de support de la base de données choisie), `Mbstring` (support UTF-8 poussé), `Tokenizer`, `XML` (parsage de XML), `Ctype`, `JSON` (parsage de JSON), `BCMath` (maths plus précises), `Intl` (support de fonctions I18n), `GD` (pour le traitement d'images ou `VIPS` préféré, installable avec `pear`), `Zip` (support des zip), `Bz2` (support du bz), `Opcache` (cache de code PHP),
-   Composer lançable dans le dossier du projet, avec l'environnement PHP ci-dessus

## Installation

-   Copier les fichiers PHP dans un répertoire servi par Nginx _(note : pour Nginx, il faut consulter la documentation ici pour avoir les "pretty-urls")_.
-   Créer un compte sur le site d'Algolia et créer une application
-   Editer dans le .env les variables suivantes :

    -   `APP_URL=`{{ adresse complète du site avec http:// }}
    -   `DB_HOST=`{{ hôte de la base de données }}
    -   `DB_PORT=`{{ port de la base de données }}
    -   `DB_DATABASE=`{{ nom de la base de données (à créer pour l'étape suivante) }}
    -   `DB_USERNAME=`{{ nom d'utilisateur de la base de données }}
    -   `DB_PASSWORD=`{{ mot de passe de l'utilisateur de la base de données }}
    -   `MAIL_*=`{{ informations du serveur SMTP }}
    -   `ALGOLIA_APP_ID=`{{ ID application Algolia }}
    -   `ALGOLIA_SECRET=`{{ secret Algolia (clé API avec écriture) }}
    -   `ALGOLIA_ITEMS_INDEX=`{{ nom de l'index Algolia contenant les items }}
    -   `ALGOLIA_BOUTIQUES_INDEX=`{{ nom de l'index Algolia contenant les boutiques }}
    -   `IMAGE_PROCESSOR=`{{ processeur d'images (`gd` par défaut, `vips` est recommandé si installé) }}

-   Lancer les commandes suivantes dans le dossier du projet :

    -   `composer install`
    -   `php artisan key:generate`
    -   `php artisan storage:link`
    -   `php artisan migrate --force` (cela crée les tables et les champs dans la base de données, automatiquement) (pour remplir la base de données avec des données fictives, ajouter `--seed`)
    -   `php artisan config:cache`
    -   `npm i`
    -   `npm run production`

-   Pour importer les données sur Algolia
    -   (`php artisan scout:flush "App\Item"`, si vous avez déjà ajouté des données pour éviter les doublons, cela efface tout)
    -   (`php artisan scout:flush "App\Boutique"`, si vous avez déjà ajouté des données pour éviter les doublons, cela efface tout)
    -   `php artisan scout:import "App\Item"`
    -   `php artisan scout:import "App\Boutique"`

### C'est prêt !

## Utilisateurs administrateurs

Pour passer un utilisateur déjà inscrit en administrateur, dans la console de la machine virtuelle (ou directement pour l'installation manuelle), utiliser la commande : `php artisan admin:set <email ou id de l'utilisateur>` .  
Aide : Pour accéder à cette console dans le cas de l'installation automatique, placez-vous dans le dossier contenant le `docker-compose.yml`, puis tapez `docker-compose exec app /bin/bash`. Vous serez ensuite propulsé dans Bash, directement dans le bon dossier.

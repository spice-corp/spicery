/*

/!**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 *!/

require('./bootstrap');

window.Vue = require('vue');

/!**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 *!/

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});
*/

import Swiper from 'swiper';
import Chart from 'chart.js';
import Leaflet from "leaflet";

require("leaflet/dist/leaflet.css");

const randomColor = require('randomcolor');
const rainbowSort = require("rainbow-sort");

const algoliasearch = require('algoliasearch/lite');
const instantsearch = require('instantsearch.js').default;
import {searchBox, infiniteHits, menuSelect} from 'instantsearch.js/es/widgets'
import {connectNumericMenu} from "instantsearch.js/es/connectors";


// Search

const searchClient = algoliasearch('B7E7GQ2B2I', '4b9087677276cebee3dcde354b128cda');
const searchInput = document.querySelector(".search_input");
var search;
if (searchInput) {
    search = instantsearch({
        indexName: searchIndex,
        searchClient,
    });
    var itemTemplate;
    if (searchMode === "items") {
        itemTemplate = `<div class="element compo_item">
                        <a href="/product/{{ id }}" alt="Aller sur la page produit de {{ name }}" class="link_item" <!--target="_blank"-->>
                            <img class="img_item" src="{{ imgthumb_src }}" alt="{{ name }}">
                            <img class="flag_item" src="/img/flags/{{countrycode}}.svg" alt="{{ countryname }}">
                            <p class="resume">{{#helpers.highlight}}{"attribute": "name"}{{/helpers.highlight}}</p>
                            <div class="price_medal">
                                <p class="item_price">{{ avg_price }}</p>
                            </div>
                        </a>
                    </div>`;
    } else {
        itemTemplate = `<div class="element compo_item">
                        <a href="/boutique/{{ id }}" alt="Aller sur la page de la boutique {{ name }}" class="link_item" <!--target="_blank"-->>
                            <img class="img_item" src="https://via.placeholder.com/300" alt="{{ name }}">
                            <img class="flag_item" src="/img/flags/{{countrycode}}.svg" alt="{{ countryname }}">
                            <p class="resume">{{#helpers.highlight}}{"attribute": "name"}{{/helpers.highlight}}</p>
                        </a>
                    </div>`;
    }
    search.addWidget(
        searchBox({
            container: ".search_input_container",
            placeholder: searchString,
            autofocus: true,
            templates: {
                loadingIndicator: ""
            },
            showReset: false,
            showSubmit: false,
            cssClasses: {
                root: "height_100",
                input: "search_input height_100",
                form: "height_100",
            }
        })
    );
    if (searchMode === "items") {
        const renderNumericMenu = (renderOptions, isFirstRender) => {
            const {
                items, hasNoResults, refine, createURL, widgetParams
            } = renderOptions;

            document.querySelector(widgetParams.container).innerHTML = `
      ${items
                .map(
                    item => `
            <option value="${item.value}"
                  ${item.isRefined ? 'selected' : ''}>
                ${item.label}
            </option>`
                )
                .join('')}
                `
            ;
            if (isFirstRender) {
                document.querySelector(widgetParams.container).addEventListener('change', event => {
                    console.log("passé");
                    refine(event.currentTarget.value);
                });
            }
        };
        const pricesNumericMenu = connectNumericMenu(
            renderNumericMenu
        );

        search.addWidget(pricesNumericMenu({
                container: '#item_price',
                attribute: 'avg_price',
                items: [
                    {label: 'Prix'},
                    {label: '0 - 5€', end: 5.00},
                    {label: '5€ - 10€', start: 5.00, end: 10.00},
                    {label: '10€ - 20€', start: 10.00, end: 20.00},
                    {label: '20€ - 50€', start: 20.00, end: 50.00},
                    {label: 'Plus de 50€', start: 50.00},
                ]
            })
        );
    }
    search.addWidget(menuSelect({
        container: '#country_menu',
        attribute: 'countryname',
        sortBy: ['isRefined', "count:desc", "name:asc"],
        templates: {
            defaultOption: 'Pays',
        },
        cssClasses: {
            root: "height_100",
            select: "height_100 last"
        }
    }));
    search.addWidget(infiniteHits({
        container: '#search_results',
        escapeHTML: true,
        templates: {
            empty: "Aucun résultat ne correspond à votre recherche <br> ¯\\_(ツ)_/¯",
            item: itemTemplate,
            showMoreText: "Afficher plus de résultats..."
        },
        cssClasses: {
            list: "algolia_list",
            loadMore: "load_more",
            emptyRoot: "empty_results",
            root: "results_root",
            item: "results_item"
        }
    }));
    search.addWidget({
        render: function (renderOptions) {
            if (renderOptions.helper.lastResults) {
                document.querySelector(".product_counter").innerHTML = renderOptions.helper.lastResults.nbHits;
            }
        }
    });
    search.start();
    let query = $_GET("product") || $_GET("boutique");
    if (query) {
        search.helper.state.query = decodeURIComponent(query);
        search.helper.search();
    }
}

const chart = document.querySelector(".boutique_graphique_pays");
if (chart !== null) {
    var myPieChart = new Chart(chart, {
        type: 'pie',
        data: {
            labels: countryStatsLabels,
            datasets: [{
                data: countryStatsValues,
                backgroundColor: sortedRandomColors(countryStatsValues.length)
            }]
        },
        options: {
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        let countryName = data.labels[tooltipItem.index] || '';
                        let percentage = Math.round(data.datasets[0].data[tooltipItem.index] || 0);
                        return `${countryName} : ${percentage} %`;
                    }
                }
            },
            plugins: {
                datalabels: {
                    formatter: function (value, context) {
                        return context.chart.data.labels[context.dataIndex];
                    }
                }
            }
        }
    });
}

const galleryElm = document.querySelector(".swiper-container");
if (galleryElm !== null) {
    var gallery = new Swiper(
        galleryElm, {
            mousewheel: true,
            loop: true,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        }
    );
}

const boutiqueMapElm = document.querySelector('#boutique-map');
if (boutiqueMapElm) {
    const boutiqueMap = Leaflet.map('boutique-map').setView(position, 15);
    Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(boutiqueMap);
    let icon = new Leaflet.Icon.Default();
    icon.options.shadowSize = [0, 0];
    Leaflet.marker(position, {icon: icon}).addTo(boutiqueMap);
}

var navBar = document.querySelector('nav');
window.onscroll = onScroll;
window.onload = adjustNavbar;

function adjustNavbar() {
    "use strict";
    if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0 >= 200) {
        navBar.classList.add("nav-colored");
    } else {
        navBar.classList.remove("nav-colored");
    }
};

function onScroll() {
    adjustNavbar();
}

function sortedRandomColors(count) {
    let colors = randomColor({
        count: count,
        alpha: 1,
        hue: "#86bf65",
        luminosity: "bright"
    });
    return rainbowSort(colors);
}

function $_GET(param) {
    var vars = {};
    window.location.href.replace(location.hash, '').replace(
        /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
        function (m, key, value) { // callback
            vars[key] = value !== undefined ? value : '';
        }
    );

    if (param) {
        return vars[param] ? vars[param] : null;
    }
    return vars;
}
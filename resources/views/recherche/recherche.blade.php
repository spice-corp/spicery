@extends('layouts.app')
@section('title', "Recherche")
@section('content')

    <header class="search-heading">
        <div class="container">
            <div class="title">
                <div class="titleProduits">
                    <a href="{{ route("products") }}"
                       class="produit_link {{isset($searchMode) ? $searchMode === "items" ? "selected" : "" : ""}}">
                        <p class="produit">PRODUITS</p></a>
                </div>
                <div class="titleBoutiques">
                    <a href="{{ route("boutiques") }}"
                       class="boutique_link {{isset($searchMode) ? $searchMode === "boutiques" ? "selected" : "" : ""}}">
                        <p class="boutique">BOUTIQUES</p></a>
                </div>
            </div>
            <form action="" class="search_form">
                <div class="search_input_container"><input type="text" class="search_input"
                                                           placeholder="Nom du produit"></div>
                @if ($searchMode && $searchMode === "items")
                    <select id="item_price">
                    </select>
                @endif
                <div id="country_menu">
                </div>
            </form>
        </div>
    </header>
    <section class="containerElement search_container ">
        <h1>
            <span class="product_counter"></span> {{isset($searchMode) ? $searchMode === "items" ? __("pages/recherche.products_found") : __("pages/recherche.boutiques_found") : ""}}
        </h1>

        <div class="wrapper" id="search_results">
        </div>

    </section>

    <script>
        var searchMode = "{{ isset($searchMode) ? $searchMode : "items" }}";
        var searchIndex =
            "{{ isset($searchMode) ? $searchMode : "items" }}{{env('APP_DEBUG') == true ? "_test" : ""}}";
        var searchString =
            "{{  isset($searchMode) ? $searchMode === "items" ? __("pages/recherche.product_placeholder") : __("pages/recherche.boutique_placeholder") : ""}}";
    </script>

@endsection


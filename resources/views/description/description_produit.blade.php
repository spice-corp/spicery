@extends('layouts.app')
@section('title', $item->name)
@section('content')

    <div class="breadcrumb_boutique">
        <a href="{{ route("products") }}">{{ __("pages/produit.back_products") }}</a>
    </div>

    <div class="div_name_produit">
        <h1 class="product-name">{{$item->name}}</h1>
    </div>

    <div class="detail_produit">
        <div class="contain_produit">
            <div class="produit_img">
                <img class="sizing" src="{{asset($item->img_src)}}">
                <div class="prix">
                    <div class="text_prix">{{ $item->averagePrice() }}</div>
                </div>
            </div>


        </div>
        <div class="description_produit">
            <h2>Description</h2>
            <div class="bar"></div>
            <p>{{$item->description}}</p>
        </div>
    </div>


    <div class="containerElement">
        <h2>Où me trouver ?</h2>

        <ul>
            @foreach($item->boutiques as $boutique)
                @include("components.boutique", [
                "img_boutique"=>isset($boutique->img_src) ? asset($boutique->img_src) : "https://via.placeholder.com/300",
                "name"=>$boutique->name,
                "link" => route("boutique_boutique", ["boutique" => $boutique->id])
                ])
            @endforeach
        </ul>
        <div class="numberPage">
            <a href="">1</a>
        </div>
    </div>

@endsection
<li class="element compo_item">
    <a href="{{ $link }}" alt="Aller sur la page produit de {{$name}}" class="link_item" {{--target="_blank"--}}>
        <img class="img_item" src="{{$img}}" alt="{{$name}}">
        <img class="flag_item" src="{{$flag_img}}" alt="{{ $countryName }}">
        <p class="resume">{{$name}}</p>
        <div class="price_medal">
            <p class="item_price">{{$price}}</p>
        </div>
    </a>
</li>
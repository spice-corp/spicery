<li class="element compo_item">
    <a href="{{ $link }}" alt="Aller sur la page de la boutique {{$name}}" class="link_item" {{--target="_blank"--}}>
        <img class="img_item" src="{{$img_boutique}}" alt="{{$name}}">
        <p class="resume">{{$name}}</p>
    </a>
</li>
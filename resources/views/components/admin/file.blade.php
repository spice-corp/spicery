<div class="form-group">
    <label class="btn btn-default">
        {{ $title }} <input type="file" id="{{ $name }}" name="{{ $name }}" hidden {{ $required ? 'required' : ''}}>
    </label>
    @if ($errors->has($name))
        <div class="invalid-feedback">
            {{ $errors->first($name) }}
        </div>
    @endif
</div>
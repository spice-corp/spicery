<div>
    <form action="{{ $route }}" method="post">
        {{ csrf_field() }}
        <button type="submit" class="btn btn-danger">Supprimer</button>
        <button type="button" class="btn" onClick="javascript:history.go(-1)">Annuler</button>
    </form>
</div>
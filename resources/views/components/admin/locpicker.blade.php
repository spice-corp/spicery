<div class="form-group">
    @include('components.admin.textarea', [
                                'title' => "Adresse",
                                'name' => 'adresse',
                                'required' => true,
                                'value' => isset($boutique->address) ? $boutique->address : ""
                                ])
    <label for="adress_search">Rechercher la position de la boutique</label>
    <div class="typeahead__container">
        <div class="typeahead__field">
            <div class="typeahead__query">
                <input id="adress_search" type="search" class="form-control"
                       name="adress_search" class="js-typeahead"
                       placeholder="Commencer à taper pour rechercher" autocomplete="off"></div>
        </div>
    </div>
    <div class="col-md-6 form-group">
        <label for="latitude">Latitude</label>
        <input id="latitude" type="number" step="0.0000001"
               class="form-control{{ $errors->has("latitude") ? ' is-invalid' : '' }}"
               name="latitude" value="{{ old("latitude", isset($lat) ? $lat : '') }}" {{ $required ? 'required' : ''}}>
        @if ($errors->has("latitude"))
            <div class="invalid-feedback">
                {{ $errors->first("latitude") }}
            </div>
        @endif
    </div>

    <div class="col-md-6 form-group">
        <label for="longitude">Longitude</label>
        <input id="longitude" type="number" step="0.0000001"
               class="form-control{{ $errors->has("longitude") ? ' is-invalid' : '' }}"
               name="longitude"
               value="{{ old("longitude", isset($lng) ? $lng : '') }}" {{ $required ? 'required' : ''}}>
        @if ($errors->has("longitude"))
            <div class="invalid-feedback">
                {{ $errors->first("longitude") }}
            </div>
        @endif
    </div>

</div>
@extends('layouts.app')
@section('content')
    <div class="all_about">
        <div class="about_contain">
            <div class="about">
                <h1>A propos</h1>
                <hr>
                <p class="about_txt">
                    Chez Spice Corp, on adore voyager ! Mais ce que l’on aime encore plus que voyager, c’est manger en
                    voyageant ! Sérieusement, qui n’aime pas découvrir de nouvelles saveurs jusqu’alors inconnues de
                    notre palais ? En tout cas, nous nous adorons !
                </p>
                <p class="about_txt"> C’est pourquoi nous avons créé Spicery, une plateforme simple et performante qui
                    répertorie les épiceries étrangères et leurs produits en Île-de-France.
                </p>
                <p class="about_txt"> Vous pouvez dès maintenant retrouver et partager les produits du monde entier,
                    mais pas seulement ! Vous pouvez aussi découvrir de nouveaux produits tous les mois, grâce à notre
                    classement des “Eats du mois”.
                </p>
                <p class="about_txt"> Spicery est aussi une aide pour les épiciers manquant de visibilité. Grâce à notre
                    site, elles sont mieux référencées et peuvent bénéficier d’aide venant de notre équipe comme des
                    vidéos, une saisie des produits dans le système, etc.
                </p>


            </div>
        </div>

        <div class="contain_us">
            <div class="element_us">
                <img class="img_us" src="/img/us/ma.jpg">

                <h2 class="titre_us">Marie-Anne Druesne</h2>
                <p class="description_us">Vous la trouverez toujours avec une tartine de confiture à la
                    figue à la main</p>

            </div>

            <div class="element_us">
                <img class="img_us" src="/img/us/Guilome.jpg">

                <h2 class="titre_us">Guillaume Mollet</h2>
                <p class="description_us">Guillaume adore la grenadine, mais attention pas n’importe laquelle, seulement
                    la <a href="{{route("product_product", ["item" => 24])}}">Small Batch Grenadine</a> (24), la vrai !
                </p>

            </div>

            <div class="element_us">
                <img class="img_us" src="/img/us/jof.jpg">

                <h2 class="titre_us">Gauffroy Ghenassia</h2>
                <p class="description_us">Un soda étranger ? Pour Geoffroy c’est le <a
                            href="{{route("product_product", ["item" => 51])}}">Mountain Dew</a> (51)
                </p>

            </div>

            <div class="element_us">
                <img class="img_us" src="/img/us/nono.jpg">

                <h2 class="titre_us">Arno Dubois</h2>
                <p class="description_us">Arno et le <a href="{{route("product_product", ["item" => 41])}}">lomo
                        fumé</a> (41), c’est une histoire d’amour

                </p>

            </div>

            <div class="element_us">
                <img class="img_us" src="/img/us/benoix.jpg">

                <h2 class="titre_us">Valentin Benoit</h2>
                <p class="description_us">Il met de la sauce <a href="{{route("product_product", ["item" => 21])}}">BBQ
                        Classic Smoky</a> (21) sur tout ce qu’il
                    trouve.

                </p>

            </div>

            <div class="element_us">
                <img class="img_us" src="/img/us/lou.jpg">

                <h2 class="titre_us">Lou Farjon</h2>
                <p class="description_us">Férue de nourriture Asiatique, elle est assoiffée de <a
                            href="{{route("product_product", ["item" => 50])}}">Pocari
                        Sweat</a> !

                </p>

            </div>


        </div>

    </div>


@endsection
@extends('adminlte::page')

@section('title', 'Edition boutiques')

@section('content_header')
    <h1>Items</h1>

    <br>
    <a href="{{ route("admin_item_new") }}">
        @component('components.admin.button')
            @lang('admin/item.create')
        @endcomponent
    </a>
@stop

@section('content')

    <table id="item-table" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Image</th>
            <th data-width="50%">Description</th>
            <th>Pays</th>
            <th>Mis à jour</th>
            <th>Créé</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($items as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->name }}
                    @if (Auth::user()->can("items.update", $item))
                        <div class="btn-group" role="group" aria-label="...">
                            <a href="{{ route("admin_item_edit", ["id" => $item->id]) }}">
                                <button type="button" class="btn btn-warning">Editer</button>
                            </a>
                            @if(Auth::user()->can('access-admin'))
                                <a href="{{ route("admin_item_delete", ["id" => $item->id]) }}">
                                    <button type="button" class="btn btn-danger">Supprimer</button>
                                </a>
                            @endif
                        </div>
                    @endif
                </td>
                <td>{{ $item->img_src }}<img src="{{ asset($item->imgthumb_src) }}" alt="" class="product-icon"></td>
                <td>{{ $item->description }}</td>
                <td>{{ $item->countrycode }}</td>
                <td>{{ $item->created_at }}</td>
                <td>{{ $item->updated_at }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@stop

@section("js")
    <script>
        $(document).ready(function () {
            $('#item-table').DataTable();
        });
    </script>
@stop

@section("css")
    <style>
        .product-icon {
            width: 10em;
            height: 10em;
            object-fit: contain;
            display: block;
            margin: 0 auto;
        }
    </style>
@stop

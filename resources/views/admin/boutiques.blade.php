@extends('adminlte::page')

@section('title', 'Edition boutiques')

@section('content_header')
    <h1>Boutiques</h1>
    <br>
    <a href="{{ route("admin_boutique_new") }}">
        @component('components.admin.button')
            @lang('admin/boutique.create')
        @endcomponent
    </a>
@stop

@section('content')

    <table id="item-table" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nom</th>
            <th data-width="50%">Description</th>
            <th>Mail</th>
            <th>Téléphone</th>
            <th>Adresse</th>
            <th>Site</th>
            <th>Lat</th>
            <th>Long</th>
            <th>Nb de produits</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($boutiques as $boutique)
            <tr>
                <td>{{ $boutique->id }}</td>
                <td>{{ $boutique->name }}
                    @if (Auth::user()->can("boutiques.update", $boutique))
                        <div class="btn-group" role="group" aria-label="...">
                            <a href="{{ route("admin_boutique_edit", ["id" => $boutique->id]) }}">
                                <button type="button" class="btn btn-warning">Editer</button>
                            </a>
                            <a href="{{ route("admin_boutique_delete", ["id" => $boutique->id]) }}">
                                <button type="button" class="btn btn-danger">Supprimer</button>
                            </a>
                        </div>
                    @endif
                </td>
                <td>{{ $boutique->description }}</td>
                <td>{{ $boutique->email }}</td>
                <td>{{ $boutique->phone }}</td>
                <td>{{ $boutique->address }}</td>
                <td>{{ $boutique->web }}</td>
                <td>{{ $boutique->lat }}</td>
                <td>{{ $boutique->lng }}</td>
                <td>
                    {{$boutique->items->count()}}
                    {{--@foreach ($boutique->tags as $tag)
                        {{$tag->name}}
                        <br>
                    @endforeach--}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@stop

@section("js")
    <script>
        $(document).ready(function () {
            $('#item-table').DataTable();
        });
    </script>
@stop

@section("css")
    <style>

    </style>
@stop

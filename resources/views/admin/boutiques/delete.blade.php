@extends('adminlte::page')

@section('title', 'Supprimer ?')

@section('content_header')

    <h1>Supprimer la boutique n°{{ $boutique->id }} : {{ $boutique->name }}</h1>

@stop

@section("content")
    @include("components.admin.question", [
        "route" => route("admin_boutique_delete", ["id" => $boutique->id])
    ])
@stop
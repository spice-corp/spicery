@extends('adminlte::page')

@section('title', 'Edition boutique')

@section('content_header')
    @if (isset($boutique))
        <h1>Editer la boutique N°{{ $boutique->id }} : {{ $boutique->name }}</h1>
    @else
        <h1>Créer une boutique</h1>
    @endif
    <br>
    <a href="{{ route("admin_boutiques") }}">Retourner à la liste des boutiques</a>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-8">
                <div class="card text-white bg-dark mb-3">
                    <div class="card-body">
                        <form method="POST"
                              action="{{ isset($boutique) ? route('admin_boutique_edit', ["item" => $boutique->id]) : route('admin_boutique_new') }}"
                              enctype="multipart/form-data">

                            {{ csrf_field() }}
                            @include('components.admin.input', [
                                'title' => __('Nom'),
                                'type' => 'text',
                                'name' => 'name',
                                'required' => true,
                                'value' => isset($boutique->name) ? $boutique->name : ""
                                ])
                            @include('components.admin.textarea', [
                                'title' => __('Description'),
                                'name' => 'description',
                                'required' => true,
                                'value' => isset($boutique->description) ? $boutique->description : ""
                                ])

                            @include('components.admin.input', [
                                'title' => __('Site web'),
                                'type' => 'text',
                                'name' => 'web',
                                'required' => false,
                                'value' => isset($boutique->web) ? $boutique->web : ""
                                ])
                            @include('components.admin.input', [
                                'title' => __('Email'),
                                'type' => 'text',
                                'name' => 'email',
                                'required' => false,
                                'value' => isset($boutique->email) ? $boutique->email : ""
                                ])
                            @include('components.admin.input', [
                                'title' => __('Téléphone'),
                                'type' => 'text',
                                'name' => 'phone',
                                'required' => true,
                                'value' => isset($boutique->phone) ? $boutique->phone : ""
                                ])

                            @include('components.admin.locpicker', [
                                'required' => true,
                                'lat' => isset($boutique->lat) ? $boutique->lat : "",
                                'lng' => isset($boutique->lng) ? $boutique->lng : "",
                                ])

                            <div class="form-group">
                                <label for="item_finder">Ajouter des produits à la boutique</label>
                                <div class="typeahead__container">
                                    <div class="typeahead__field">
                                        <div class="typeahead__query">
                                            <input id="item_finder" type="search" class="form-control"
                                                   name="item_finder" class="js-typeahead"
                                                   placeholder="Commencer à taper pour rechercher" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <table id="item-table" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nom</th>
                                    <th>Prix</th>
                                    <th data-orderable="false">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (old("items", isset($boutique->items) ? $boutique->items : false))
                                    @foreach(old("items", isset($boutique->items) ? $boutique->items : []) as $item)
                                        <tr>
                                            <td>{{ isset($boutique) ? $item->id : $item["id"] }}<input
                                                        type="hidden"
                                                        value="{{ isset($boutique) ? $item->id : $item["id"] }}"
                                                        name="items[{{ isset($boutique) ? $item->id : $item["id"] }}][id]">
                                            </td>
                                            <td>{{ isset($boutique) ? $item->name : $item["name"] }}<input
                                                        type="hidden"
                                                        value="{{ isset($boutique) ? $item->name : $item["name"] }}"
                                                        name="items[{{ isset($boutique) ? $item->id : $item["id"] }}][name]">
                                            </td>
                                            <td><input type="number" step="0.01"
                                                       value="{{ isset($boutique) ? $item->pivot->price : $item["price"] }}"
                                                       name="items[{{ isset($boutique) ? $item->id : $item["id"] }}][price]">€
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-danger">Supprimer</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif

                                </tbody>
                            </table>

                            {{--<div class="form-group">
                                <label for="item_finder">Tags</label>
                                <div class="typeahead__container">
                                    <div class="typeahead__field">
                                        <div class="typeahead__query">
                                            <input id="item_finder" type="search" class="form-control"
                                                   name="item_finder" class="js-typeahead"
                                                   placeholder="Commencer à taper pour rechercher"
                                                   autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>--}}

                            @component('components.admin.button')
                                @lang('Enregistrer')
                            @endcomponent

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section("css")
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/jquery-typeahead/2.10.6/jquery.typeahead.min.css"
          integrity="sha256-CU0/rbxVB3Eixd3bbIuJxHJLDnXriJS9cwp/BfcgpLw=" crossorigin="anonymous"/>
@endsection

@section("js")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-typeahead/2.10.6/jquery.typeahead.min.js"
            integrity="sha256-W+Cxk9exgjON2p73M4RcoKvCpQUZ+IjXhEzZk6rlg9M=" crossorigin="anonymous"></script>
    <script>
        var itemList = {!! $itemList->toJson() !!};
    </script>
    <script>

        $.typeahead({
            input: "#adress_search",
            order: "asc",
            dynamic: true,
            hint: true,
            display: ["display_name"],
            filter: false,
            source: {
                places: {
                    // Ajax Request
                    ajax: {
                        url: "https://nominatim.openstreetmap.org/search?q=@{{query}}&format=jsonv2"
                    }
                }
            },
            callback: {
                onClickAfter: function (node, a, item, event) {
                    $("#longitude").val(item.lon);
                    $("#latitude").val(item.lat);
                }
            }
        });

        $.typeahead({
            input: "#item_finder",
            order: "asc",
            dynamic: true,
            hint: true,
            minLength: 0,
            display: ["countrycode", "name", "id"],
            template: "[@{{countrycode}}] - @{{name}}",
            source: {
                data: itemList
            },
            callback: {
                onClickAfter: function (node, a, item, event) {
                    node[0].value = "";
                    $('#item-table button').off("click", deleteItem);
                    $('#item-table').DataTable().row.add([item.id + '<input type="hidden" value="' + item.id + '" name="items[' + item.id + '][id]">', item.name + '<input type="hidden" value="' + item.name + '" name="items[' + item.id + '][name]">', '<input type="number" step="0.01" value="0" name="items[' + item.id + '][price]">€', '<button type="button" class="btn btn-danger">Supprimer</button>']).draw().node();
                    $('#item-table button').one("click", deleteItem);
                }
            }
        });

        $(document).ready(function () {
            $('#item-table').DataTable();
            $('#item-table button').one("click", deleteItem);
        });

        function deleteItem(event) {
            $(this).off("click", deleteItem);
            $('#item-table').DataTable().row($(this).parents("tr")).remove().draw();
        }

    </script>
@endsection
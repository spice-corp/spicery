@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Bienvenue sur l'interface d'administration !</h1>
@stop

@section('content')
    <p>Toutes les options qui vous sont offertes sont disponibles dans le panneau de navigation sur votre gauche</p>
@stop
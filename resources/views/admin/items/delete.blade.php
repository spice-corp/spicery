@extends('adminlte::page')

@section('title', 'Supprimer ?')

@section('content_header')

    <h1>Supprimer l'item n°{{ $item->id }} : {{ $item->name }}</h1>

@stop

@section("content")
    @include("components.admin.question", [
        "route" => route("admin_item_delete", ["id" => $item->id])
    ])
@stop
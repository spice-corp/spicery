@extends('adminlte::page')

@section('title', 'Edition article')

@section('content_header')
    @if (isset($item))
        <h1>Editer l'article N°{{ $item->id }} : {{ $item->name }}</h1>
    @else
        <h1>Créer un article</h1>
    @endif
    <br>
    <a href="{{ route("admin_items") }}">Retourner à la liste des items</a>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-8">
                <div class="card text-white bg-dark mb-3">
                    <div class="card-body">
                        <form method="POST"
                              action="{{ isset($item) ? route('admin_item_edit', ["item" => $item->id]) : route('admin_item_new') }}"
                              enctype="multipart/form-data">

                            {{ csrf_field() }}
                            @include('components.admin.input', [
                                'title' => __('Nom'),
                                'type' => 'text',
                                'name' => 'name',
                                'required' => true,
                                'value' => isset($item->name) ? $item->name : ""
                                ])
                            @include('components.admin.textarea', [
                                'title' => __('Description'),
                                'name' => 'description',
                                'required' => true,
                                'value' => isset($item->description) ? $item->description : ""
                                ])
                            @include('components.admin.input', [
                                'title' => __('Code pays (2 lettres)'),
                                'type' => 'text',
                                'name' => 'countrycode',
                                'required' => true,
                                'value' => isset($item->countrycode) ? $item->countrycode : ""
                                ])

                            <div class="form-group">
                                @if (isset($item) && $item->img_src)
                                    <p>Image actuelle :</p>
                                    <img style="width: 100%" src="{{asset($item->img_src)}}">
                                @else
                                    <p>Aucune image stockée actuellement</p>
                                @endif
                            </div>
                            @include('components.admin.file', [
                                'title' => __('Envoi'),
                                'name' => 'image',
                                'required' => isset($item->img_src) ? false : true
                                ])

                            @component('components.admin.button')
                                @lang('Enregistrer')
                            @endcomponent

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('images/icons/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('images/icons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/icons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('images/icons/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('images/icons/safari-pinned-tab.svg')}}" color="#d8ccd0">
    <link rel="shortcut icon" href="{{asset('images/icons/favicon.ico')}}">
    <meta name="msapplication-TileColor" content="#d8ccd0">
    <meta name="msapplication-config" content="{{asset('images/icons/browserconfig.xml')}}">
    <meta name="theme-color" content="#d8ccd0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Spicery - @yield('title', 'Des saveurs étrangères à portée d\'épiceries')</title>
    <meta name="description"
          content="Des saveurs étrangères à portée d'épiceries ! Venez découvrir la sélection des produits de nos épiceries partenaires, recherchez parmi nos produits et nos pays, pour trouver LE produit étranger que vous ne trouviez pas.">

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    {{--<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        <li class="nav-item">
                            @if (Route::has('register'))
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            @endif
                        </li>
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>--}}

    <nav class="nav">
        <div class="nomSpice"><a href="{{ route("home") }}">spicery</a></div>
        <ul>
            <li><a href="{{ route("products") }}">{{ __("pages/app.nav1") }}</a></li>
            <li><a href="{{ route("boutiques") }}">{{ __("pages/app.nav2") }}</a></li>
        </ul>
    </nav>

    <main>
        @yield('content')
    </main>
    <footer>
        <div class="footer-container">
            <div class="navintra">
                @if(Auth::check())
                    <a href="{{ route("admin") }}">
                        <h3>
                            <span>{{ __("pages/app.admin") }}</span>
                        </h3>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                    <a href="{{ route("logout") }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <h3>
                            <span>{{ __("pages/app.logout") }}</span>
                        </h3>
                    </a>
                @else
                    <a href="{{ route("login") }}">
                        <h3>
                            <span>{{ __("pages/app.footer1") }}</span>
                        </h3>
                    </a>
                @endif

                <a href="{{ route("home") }}">
                    <h3>
                        <span>{{ __("pages/app.footer2") }}</span>
                    </h3>
                </a>
                <a href="{{ route("about") }}">
                    <h3>
                        <span>{{ __("pages/app.footer3") }}</span>
                    </h3>
                </a>
            </div>
            <div class="follow">
                <h3>{{ __("pages/app.follow") }}</h3>
                <div class="socialmedia">
                    <a href="https://www.instagram.com/spicery.epiceries/?hl=fr" target="_blank" rel="noreferrer"
                       title="Page Instagram">
                        <div class="instagram"></div>
                    </a>
                    <a href="https://www.facebook.com/Spicery.epiceries/" target="_blank" rel="noreferrer"
                       title="Page Facebook">
                        <div class="facebook"></div>
                    </a>
                </div>
            </div>
        </div>
    </footer>
</div>
</body>
</html>

@extends('layouts.app')
@section('content')

    <header class="header-container">
        <div class="content">
            <div class="recherche">
                <div>
                    <img class="logo" src="/img/svgs/Logo.svg" alt="">
                </div>
                <em class="slogan">@lang("pages/accueil.slogan")</em>
                <form action="{{ route("search") }}" class="recherche-formulaire">
                    {{ csrf_field() }}
                    <div class="bondisplay">
                        <div class="colle">
                            <input class="champ1" type="text"
                                   placeholder="{{ __("pages/accueil.search_product") }}" name="product"/>
                            <input class="champ2" type="text" placeholder="{{ __("pages/accueil.search_shop") }}"
                                   name="boutique"/>
                        </div>
                    </div>
                    <div class="posearch"><input class="bouton" type="submit" value="" aria-label="Rechercher"></div>
                </form>

                <a class="btn-randomproduct vertical-center-content"
                   href="{{ route("product_random") }}"><p
                    >{{ __("pages/accueil.random_product") }}</p></a>
            </div>
            <img class="base_palmier" src="/img/svgs/base_palmier.svg" alt="">
        </div>
    </header>
    <div class="top3 pgd1">

        <h1>{{ __("pages/accueil.title_eats") }}</h1>
        <div class="eats">
            @foreach($eats as $eat)
                <div><p>{{ $eat->title }}</p></div>
            @endforeach
        </div>
    </div>


@endsection
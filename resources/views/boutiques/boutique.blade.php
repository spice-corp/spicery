@extends("layouts.app")
@section('title', 'Boutique : '.$boutique->name)

@section("content")
    <div class="breadcrumb_boutique">
        <a href="{{ route("boutiques") }}">{{ __("pages/boutique.back_groceries") }}</a>
    </div>
    <div class="div_name_boutique">
        <h1>{{$boutique -> name}}</h1>
    </div>
    <div class="container_boutique">
        <div class="info_boutique">
            @if ($boutique->email)
                <span class="email_boutique">{{$boutique -> email}}</span>
            @endif
            @if ($boutique->phone)
                <span class="tel_boutique">{{$boutique -> phone}}</span>
            @endif

            <span class="adresse_boutique">{{$boutique -> address}}</span>
        </div>
        <div class="div_tags">
            @foreach($boutique->tags as $tag)
                <span class="div_tag">$tag</span>
            @endforeach
        </div>
        <div class="detail_boutique">
            <div class="description_boutique">
                <h2>{{ __("pages/boutique.description") }}</h2>
                <div class="bar"></div>
                <p>{{$boutique -> description}}</p>
            </div>
            <div class="horaire_boutique">
                <h2>{{ __("pages/boutique.schedule") }}</h2>
                <div class="bar"></div>
                <div class="horaires_jours">
                    <div class="horaire_jour">
                        <div>{{ __("pages/boutique.monday") }}</div>
                        <div>Fermé</div>
                    </div>
                    <div class="horaire_jour">
                        <div>{{ __("pages/boutique.tuesday") }}</div>
                        <div>9h00 - 12h00 / 14h00 - 19h00</div>
                    </div>
                    <div class="horaire_jour">
                        <div>{{ __("pages/boutique.wednesday") }}</div>
                        <div>9h00 - 12h00 / 14h00 - 19h00</div>
                    </div>
                    <div class="horaire_jour">
                        <div>{{ __("pages/boutique.thursday") }}</div>
                        <div>9h00 - 12h00 / 14h00 - 19h00</div>
                    </div>
                    <div class="horaire_jour">
                        <div>{{ __("pages/boutique.friday") }}</div>
                        <div>9h00 - 12h00 / 14h00 - 19h00</div>
                    </div>
                    <div class="horaire_jour">
                        <div>{{ __("pages/boutique.saturday") }}</div>
                        <div>9h00 - 19h00</div>
                    </div>
                    <div class="horaire_jour dimanche">
                        <div>{{ __("pages/boutique.sunday") }}</div>
                        <div>Fermé</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="detail_visuel_boutique">
            <div class="gallery_boutique">
                <div>
                    <h2>{{ __("pages/boutique.gallery") }}</h2>
                    <div class="bar"></div>
                </div>
                <div class="boutique_gallery_container">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <img src="https://placeimg.com/640/480/tech" alt="" class="swiper-slide">
                            <img src="https://placeimg.com/640/480/tech" alt="" class="swiper-slide">
                            <img src="https://placeimg.com/640/480/tech" alt="" class="swiper-slide">
                        </div>
                        <div class="swiper-pagination"></div>

                        <!-- If we need navigation buttons -->
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>

                    </div>
                </div>

            </div>
            <div class="boutique_graphique_pays_container">
                <canvas class="boutique_graphique_pays">
                </canvas>
            </div>
            <div class="map_boutique">
                <div class="map">
                    <h2>{{ __("pages/boutique.map") }}</h2>
                    <div class="bar"></div>
                </div>
                <div class="boutique_map" id="boutique-map"></div>
            </div>
            <div>

            </div>
        </div>
        <div class="containerElement items_list">
            <h2>{{ __("pages/boutique.products") }}</h2>
            <div class="bar"></div>
            <ul>
                @foreach($items as $item)
                    @include("components.item", [
                        "name" => $item->name,
                        "link" => route("product_product", ["item" => $item->id]),
                        "img" => asset($item->imgthumb_src),
                        "price" => $item->pivot->price,
                        "flag_img" => asset("/img/flags/".strtolower($item->countrycode).".svg"),
                        "countryName" => $item->countryname()
                    ])
                @endforeach
            </ul>
            <div class="numberPage">
                <a href="">1</a>
            </div>
        </div>

        <script>
            const countryStatsLabels = {!! $countryStatsLabels !!};
            const countryStatsValues = {!! $countryStatsValues !!};
            const position = [{{$boutique->lat}}, {{ $boutique->lng }}];
        </script>

@endsection
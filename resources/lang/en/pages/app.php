<?php
/**
 * Created by PhpStorm.
 * User: Marie Anne
 * Date: 07/02/2019
 * Time: 15:42
 */

return [
    "nav1" => "products",
    "nav2" => "groceries",
    "footer1" => "Login",
    "footer2" => "Home",
    "footer3" => "About",
    "follow" => "Follow us :",
    "logout" => "Logout",
    "admin" => "Administration panel"
];
<?php
/**
 * Created by PhpStorm.
 * User: Marie Anne
 * Date: 07/02/2019
 * Time: 15:42
 */

return [
    "slogan" => "<strong>Find</strong> a <strong>product</strong> or a <strong>grocery</strong><br/> that suits your <strong>taste</strong>",
    "search_product" => "by product",
    "search_shop" => "by grocery",
    "random_product" => "Random products",
    "title_eats" => "TOP 3 Eats"

];
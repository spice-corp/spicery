<?php

return [
    "product_placeholder" => "Nom du produit",
    "boutique_placeholder" => "Nom de la boutique",
    "products_found" => "produits trouvés",
    "boutiques_found" => "boutiques trouvées",
];

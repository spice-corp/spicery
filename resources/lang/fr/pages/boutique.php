<?php

return [
    "back_groceries" => "Retour aux boutiques",
    "description" => "Description",
    "schedule" => "Horaires",
    "gallery" => "Galerie",
    "map" => "Carte",
    "products" => "Leurs produits",
    "monday" => "lundi",
    "tuesday" => "mardi",
    "wednesday" => "mercredi",
    "thursday" => "jeudi",
    "friday" => "vendredi",
    "saturday" => "samedi",
    "sunday" => "dimanche",

];
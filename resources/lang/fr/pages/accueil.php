<?php
/**
 * Created by PhpStorm.
 * User: Arno
 * Date: 07/02/2019
 * Time: 13:59
 */
return [
    "slogan" => "<strong>Recherchez</strong> un <strong>produit</strong> ou une <strong>épicerie </strong><br/> qui vous rapproche de vos <strong>envies</strong>",
    "search_product" => "par nom de produit",
    "search_shop" => "par nom de boutique",
    "random_product" => "Produit aléatoire",
    "title_eats" => "TOP 3 des Eats"

];
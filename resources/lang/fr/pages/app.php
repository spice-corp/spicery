<?php

return [
    "nav1" => "produits",
    "nav2" => "boutiques",
    "footer1" => "Connexion",
    "footer2" => "Accueil",
    "footer3" => "A propos",
    "follow" => "Suivez-nous :",
    "logout" => "Déconnexion",
    "admin" => "Espace d'administration"
];
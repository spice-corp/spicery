<?php

use Illuminate\Database\Seeder;

class BoutiquesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Boutique::class, 20)->create()->each(function ($boutique) {
            $boutique->owners()->attach(App\User::inRandomOrder()->first());
            $boutique->save();
        });
    }
}

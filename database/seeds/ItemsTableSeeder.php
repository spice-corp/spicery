<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Item::class, 150)->create()->each(function ($item) {
            $faker = \Faker\Factory::create();
            $item->boutiques()->attach(App\Boutique::inRandomOrder()->first(), ["price" => $faker->randomFloat(2)]);
            $item->save();
        });
    }
}

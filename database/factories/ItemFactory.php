<?php

use Faker\Generator as Faker;

$factory->define(App\Item::class, function (Faker $faker) {
    return [
        'name' => $faker->lexify('?????? ??????'),
        'img_src' => $faker->imageUrl(700, 400, 'cats'),
        'imgthumb_src' => $faker->imageUrl(300, 300, 'cats'),
        'description' => $faker->paragraph(4, true),
        'countrycode' => $faker->countryCode(),
        'owner' => App\User::inRandomOrder()->first()->id,
        'created_at' => $faker->dateTime($max = 'now'),
        'updated_at' => $faker->dateTime($max = 'now')
    ];
});

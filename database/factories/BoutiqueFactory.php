<?php

use Faker\Generator as Faker;

$factory->define(App\Boutique::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'email' => $faker->companyEmail,
        'phone' => $faker->phoneNumber,
        'description' => $faker->paragraph,
        'address' => $faker->address,
        'lat' => $faker->latitude,
        'lng' => $faker->longitude,
        "web" => $faker->domainName
    ];
});

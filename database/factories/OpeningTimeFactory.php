<?php

use Faker\Generator as Faker;

$factory->define(App\OpeningTime::class, function (Faker $faker) {
    return [
        'boutique_id' => function () {
            return factory(App\Boutique::class)->create()->id;
        },
        'weekday' => $faker->numberBetween(1, 7),
        'opening' => $faker->time('H:i:s'),
        'closing' => $faker->time('H:i:s'),
    ];
});

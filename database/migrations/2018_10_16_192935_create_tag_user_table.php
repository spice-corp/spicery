<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boutique_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("boutique_id")->unsigned();
            $table->foreign('boutique_id')->references('id')->on('boutiques')->onDelete('cascade');
            $table->integer("tag_id")->unsigned();
            $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tag_user');
    }
}

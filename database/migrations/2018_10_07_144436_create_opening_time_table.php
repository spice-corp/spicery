<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpeningTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opening_time', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("boutique_id")->unsigned();
            $table->foreign('boutique_id')->references('id')->on('boutiques')->onDelete('cascade');
            $table->integer('weekday');
            $table->time("opening");
            $table->time("closing");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opening_time');
    }
}

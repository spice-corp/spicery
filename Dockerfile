FROM php:7-apache-stretch

ARG FOLDER
ENV FOLDER $FOLDER
RUN mkdir ${FOLDER}
WORKDIR $FOLDER


COPY ./spicery_app $FOLDER
RUN mv /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini

RUN apt-get update -yqq && apt-get install -yqq g++ libicu-dev libmcrypt-dev libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev unixodbc-dev libpq-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev libzip-dev libvips libvips-dev gnupg2 unzip git wait-for-it
RUN docker-php-ext-install -j$(nproc) bcmath pdo_mysql intl gd zip bz2 opcache
RUN pecl install vips && docker-php-ext-enable vips
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - && apt-get install -yqq nodejs
RUN npm install && npm run prod
RUN curl -sS https://getcomposer.org/installer | php && php composer.phar install -n --ansi --optimize-autoloader --no-dev
#RUN curl -sS https://getcomposer.org/installer | php && php composer.phar install -n --ansi
RUN a2enmod rewrite && a2enmod http2
RUN php artisan config:clear && php artisan storage:link && php artisan view:clear
RUN find . -not -path "*/node_modules*" -not -path "*/vendor" -type f -exec chown www-data:www-data {} \;
RUN find . -not -path "*/node_modules*" -not -path "*/vendor" -type f -exec chmod 775 {} \;
RUN sed -ri -e 's!/var/www/html!${PUBLIC_HTML}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${FOLDER}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN apt-get remove -yqq g++ nodejs libicu-dev libmcrypt-dev libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev unixodbc-dev libpq-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev gnupg2

#CMD [ "php", "artisan", "queue:work", "--sleep=3", "--tries=3" ]